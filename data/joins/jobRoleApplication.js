module.exports = [
  // UI 1
  {
    jraJobRole: 'job_2|role_1',
    jraFreelancerId: 'erdy',
    shortlisted: true,
    removed: false,
  },
  {
    jraJobRole: 'job_2|role_1',
    jraFreelancerId: 'thofhans',
    shortlisted: false,
    removed: true,
  },
  {
    jraJobRole: 'job_2|role_1',
    jraFreelancerId: 'erwin',
    shortlisted: true,
    removed: false,
  },
  {
    jraJobRole: 'job_2|role_1',
    jraFreelancerId: 'flashzapper',
    shortlisted: false,
    removed: false,
  },
  {
    jraJobRole: 'job_2|role_1',
    jraFreelancerId: 'faizaldwinugraha',
    shortlisted: false,
    removed: false,
  },
  // UX 2
  {
    jraJobRole: 'job_2|role_2',
    jraFreelancerId: 'erdy',
    shortlisted: true,
    removed: false,
  },
  {
    jraJobRole: 'job_2|role_2',
    jraFreelancerId: 'thofhans',
    shortlisted: true,
    removed: false,
  },
  {
    jraJobRole: 'job_2|role_2',
    jraFreelancerId: 'erwin',
    shortlisted: false,
    removed: false,
  },
  {
    jraJobRole: 'job_2|role_2',
    jraFreelancerId: 'flashzapper',
    shortlisted: false,
    removed: true,
  },
]
