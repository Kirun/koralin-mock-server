module.exports = [
  // JOB 1
  {
    jobRole: 'job_1|role_1',
    qualification: 'user_research',
    level: 2,
    isOptional: false,
  },

  // JOB 2 (draft)
  {
    jobRole: 'job_2|role_1--draft',
    qualification: 'ui_design',
    level: 1,
    isOptional: false,
  },
  {
    jobRole: 'job_2|role_2--draft',
    qualification: 'ux_design',
    level: 2,
    isOptional: true,
  },
  {
    jobRole: 'job_2|role_2--draft',
    qualification: 'po',
    level: 2,
    isOptional: false,
  },
  // JOB 2 (main)
  {
    jobRole: 'job_2|role_1',
    qualification: 'ui_design',
    level: 1,
    isOptional: false,
  },
  {
    jobRole: 'job_2|role_2',
    qualification: 'ux_design',
    level: 2,
    isOptional: true,
  },

  // JOB 3
  {
    jobRole: 'job_3|role_1',
    qualification: 'ui_design',
    level: 1,
    isOptional: false,
  },
  {
    jobRole: 'job_3|role_2',
    qualification: 'unity',
    level: 2,
    isOptional: false,
  },
  {
    jobRole: 'job_3|role_2',
    qualification: 'c#',
    level: 2,
    isOptional: false,
  },
]
