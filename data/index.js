const fs = require('fs')
const path = require('path')


const classes = {}
const joins = {}

// collect data
fs.readdirSync(path.join(__dirname, '/classes'))
  .forEach((file) => {
    file = path.basename(file,'.js')
    classes[file] = require('./classes/'+file)
  })

fs.readdirSync(path.join(__dirname, '/joins'))
  .forEach((file) => {
    file = path.basename(file,'.js')
    joins[file] = require('./joins/'+file)
  })



module.exports = {
  classes,
  joins,
}
