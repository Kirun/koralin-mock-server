module.exports = [
  // JOB 1
  {
    id: 'job_1|role_1',
    jobId: 'job_1',
    name: 'User Researcher',
    description: '<p>The job is <strong>researching users</strong>, blablabla</p>',
    needed: 3,
  },

  // JOB 2 (draft)
  {
    id: 'job_2|role_1--draft',
    jobId: 'job_2--draft',
    name: 'UI Designer',
    description: '<p>The job is <strong>designing UI</strong>, Lorem ipsum dolor sit amet</p>',
    needed: 2,
  },
  {
    id: 'job_2|role_2--draft',
    jobId: 'job_2--draft',
    name: 'UX Designer',
    description: '<p>The job is <strong>designing UX</strong>, Lorem ipsum dolor sit amet</p>',
    needed: 2,
  },
  {
    id: 'job_2|role_3--draft',
    jobId: 'job_2--draft',
    name: 'PO',
    description: '<p>The job is <strong>PO-ing</strong>, Lorem ipsum dolor sit amet</p>',
    needed: 1,
  },
  // JOB 2 (published)
  {
    id: 'job_2|role_1',
    jobId: 'job_2',
    name: 'UI Designer',
    description: '<p>The job is <strong>designing UI</strong>, blablabla</p>',
    needed: 2,
  },
  {
    id: 'job_2|role_2',
    jobId: 'job_2',
    name: 'UX Designer',
    description: '<p>The job is <strong>designing UX</strong>, blablabla</p>',
    needed: 2,
  },

  // JOB 3
  {
    id: 'job_3|role_1',
    jobId: 'job_3',
    name: 'UI Designer',
    description: '<p>The job is <strong>designing UI</strong>, blablabla</p>',
    needed: 1,
  },
  {
    id: 'job_3|role_2',
    jobId: 'job_3',
    name: 'Unity Developer',
    description: '<p>The job is <strong>developing an app in Unity</strong>, blablabla</p>',
    needed: 2,
  },
  {
    id: 'job_3|role_3',
    jobId: 'job_3',
    name: 'Elephant Wrangler',
    description: '<p>The job is <strong>wrangling elephants</strong>, blablabla</p>',
    needed: 1,
  },
]
