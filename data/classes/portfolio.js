module.exports = [
  {
    url: 'linkedin.com/in/erdy',
    freelancerId: 'erdy',
    online: true,
    type: 'linkedin',
  },
  {
    url: 'erdy_portfolio.pdf',
    freelancerId: 'erdy',
    online: false,
    type: 'pdf',
  },
  {
    url: 'erdy_CV.doc',
    freelancerId: 'erdy',
    online: false,
    type: 'doc',
  },
]
