module.exports = [
  // PROGRAMMING
  {
    category: 'programming',
    id: 'c#',
    l_name: 'c#',
    name: 'C#',
  },
  {
    category: 'programming',
    id: 'c++',
    l_name: 'c++',
    name: 'C++',
  },
  {
    category: 'programming',
    id: 'java',
    l_name: 'java',
    name: 'Java',
  },
  {
    category: 'programming',
    id: 'javascript',
    l_name: 'javascript',
    name: 'Javascript',
  },
  {
    category: 'programming',
    id: 'ios_dev',
    l_name: 'ios dev',
    name: 'iOS Development',
  },
  {
    category: 'programming',
    id: 'android_dev',
    l_name: 'android dev',
    name: 'Android Development',
  },
  {
    category: 'programming',
    id: 'web_dev',
    l_name: 'web dev',
    name: 'Web Development',
  },
  {
    category: 'programming',
    id: 'frontend_dev',
    l_name: 'frontend dev',
    name: 'Frontend Development',
  },
  {
    category: 'programming',
    id: 'backend_dev',
    l_name: 'backend dev',
    name: 'Backend Development',
  },
  {
    category: 'programming',
    id: 'unity',
    l_name: 'unity',
    name: 'Unity',
  },
  {
    category: 'programming',
    id: 'unreal',
    l_name: 'unreal',
    name: 'Unreal',
  },
  // ART & DESIGN
  {
    category: 'art & design',
    id: 'ui_design',
    l_name: 'ui design',
    name: 'UI Design',
  },
  {
    category: 'art & design',
    id: 'ux_design',
    l_name: 'ux design',
    name: 'UX Design',
  },

  {
    category: 'art & design',
    id: 'illustration',
    l_name: 'illustration',
    name: 'Illustration',
  },
  {
    category: 'art & design',
    id: 'branding',
    l_name: 'branding',
    name: 'Branding',
  },

  {
    category: 'art & design',
    id: 'script_writing',
    l_name: 'script writing',
    name: 'Script Writing',
  },
  {
    category: 'art & design',
    id: 'copy_writing',
    l_name: 'copy writing',
    name: 'Copy Writing',
  },

  {
    category: 'art & design',
    id: '2d_animation',
    l_name: '2d animation',
    name: '2D Animation',
  },
  {
    category: 'art & design',
    id: 'character_design',
    l_name: 'character design',
    name: 'Character Design',
  },

  {
    category: 'art & design',
    id: '3d_animation',
    l_name: '3d animation',
    name: '3D Animation',
  },
  {
    category: 'art & design',
    id: '3d_modelling',
    l_name: '3d modelling',
    name: '3D Modelling',
  },
  {
    category: 'art & design',
    id: '3d_character_modelling',
    l_name: '3d character modelling',
    name: '3D Character Modelling',
  },
  {
    category: 'art & design',
    id: '3d_character_animation',
    l_name: '3d character animation',
    name: '3D Character Animation',
  },
  // MANAGEMENT
  {
    category: 'management',
    id: 'po',
    l_name: 'po',
    name: 'PO',
  },
  {
    category: 'management',
    id: 'sm',
    l_name: 'sm',
    name: 'SM',
  },
  // OTHERS
  {
    category: 'others',
    id: 'motion_capture',
    l_name: 'motion capture',
    name: 'Motion capture',
  },
  {
    category: 'others',
    id: 'data_science',
    l_name: 'data science',
    name: 'Data Science',
  },
  {
    category: 'others',
    id: 'user_research',
    l_name: 'user research',
    name: 'User Research',
  },
]
