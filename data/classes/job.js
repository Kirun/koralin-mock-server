module.exports = [
  // JOB 1
  {
    id: 'job_1',
    name: 'Job 1',
    client: 'client A',
    type: 'user research',

    status: 'draft',

    description: '<p>description of <strong>job 1</strong></p>',

    startDate: new Date(Date.parse('20 Jun 2018')),
    endDate: new Date(Date.parse('28 Jul 2018')),
  },
  // JOB 2 (draft)
  {
    id: 'job_2--draft',
    name: 'Job 2 changed name',
    client: 'client B',
    type: 'sprint',

    status: 'draft',
    publishedAt: new Date(Date.parse('8 May 2018')),

    description: '<p>description of <strong>job 2</strong> Lorem ipsum dolor sit amet</p>',

    startDate: new Date(Date.parse('12 Jun 2018')),
    endDate: new Date(Date.parse('4 Jul 2018')),
  },
  // JOB 2 (published)
  {
    id: 'job_2',
    name: 'Job 2',
    client: 'client B',
    type: 'sprint',

    status: 'published',

    description: '<p>description of <strong>job 2</strong></p>',

    startDate: new Date(Date.parse('22 Jun 2018')),
    endDate: new Date(Date.parse('24 Jul 2018')),
  },
  // JOB 3
  {
    id: 'job_3',
    name: 'Job 3',
    client: 'client C',
    type: 'sprint',

    status: 'published',

    description: '<p>description of <strong>job 3</strong></p>',

    startDate: new Date(Date.parse('12 Jun 2018')),
    endDate: new Date(Date.parse('20 Jul 2018')),
  },
]
