const gapiHelper = require('./gapi')
const {google} = require('googleapis')
const drive = google.drive('v3')

const _ = require('lodash')



async function getFolderStructure () {

  // const q = [
  //   "trashed = false"
  // ]
  // // if(lastModified)
  // //   q.push(`modifiedTime > "${lastModified.toISOString()}"`)
  //
  // const res = await gapiHelper.call(
  //   drive.files.list,
  //   {
  //     fields:[
  //       'files/id',
  //       'files/name',
  //       'files/parents',
  //       'files/modifiedTime',
  //       'files/mimeType',
  //     ].join(','),
  //
  //     q: q.join(' and ')
  //   }
  // )
  // .catch((err) => console.log(err))
  // const filesList = res.files
  // console.log(JSON.stringify(filesList, undefined, 2));

  const filesList =
  [
    {
      "id": "1XzHmJfYEOny2Bv6Oqqe_GjVjuZEz6TQe6daN3WYpxt0",
      "name": "Projects' Freelancers' Qualifications",
      "mimeType": "application/vnd.google-apps.spreadsheet",
      "parents": [
        "1R74BuyEhQfzOi3-wF0IwkCNx4NsPdX_-"
      ],
      "modifiedTime": "2018-04-16T18:58:37.699Z"
    },
    {
      "id": "1XBvvpk4-RVsXVaRWCBrQUleLVEpsJ-I7",
      "name": "Sample Project",
      "mimeType": "application/vnd.google-apps.folder",
      "parents": [
        "1U0FeMWxrMo4Yo_KIGCmFhVsVdzBxkqcU"
      ],
      "modifiedTime": "2018-04-12T06:39:18.300Z"
    },
    {
      "id": "1AJZnZswAXtmtxjYBjfbKR_bjjJA7Lohz",
      "name": "(ph) Sprint 1",
      "mimeType": "application/vnd.google-apps.folder",
      "parents": [
        "1XBvvpk4-RVsXVaRWCBrQUleLVEpsJ-I7"
      ],
      "modifiedTime": "2018-04-11T20:17:00.534Z"
    },
    {
      "id": "18Ij4fD_WZCVz0PstpUCO-3lpZ9KBulbh",
      "name": "Empty Project",
      "mimeType": "application/vnd.google-apps.folder",
      "parents": [
        "1U0FeMWxrMo4Yo_KIGCmFhVsVdzBxkqcU"
      ],
      "modifiedTime": "2018-04-11T17:53:17.906Z"
    },
    {
      "id": "1AW1V4ut04iYCfio8XqMStDszhHoIFNA3kXlXVnaCdeM",
      "name": "Copy of Sample Project Application",
      "mimeType": "application/vnd.google-apps.form",
      "parents": [
        "1XBvvpk4-RVsXVaRWCBrQUleLVEpsJ-I7"
      ],
      "modifiedTime": "2018-04-11T06:15:44.903Z"
    },
    {
      "id": "1nF3XspfCCujCaPZlVL-tewFPy0buBnlA6gOU0Hu07Uw",
      "name": "Copy of Copy of Freelancing form",
      "mimeType": "application/vnd.google-apps.form",
      "parents": [
        "1KW1B5_XkdFGyOzTxPAPy8o6WVcyUoxxj"
      ],
      "modifiedTime": "2018-04-11T06:15:28.630Z"
    },
    {
      "id": "1ePExdRmJQ6mfQDZznO42I1LrvvCCQP4pSawp6v78vyw",
      "name": "Role invitation",
      "mimeType": "application/vnd.google-apps.spreadsheet",
      "parents": [
        "1XBvvpk4-RVsXVaRWCBrQUleLVEpsJ-I7"
      ],
      "modifiedTime": "2018-04-09T11:10:18.511Z"
    },
    {
      "id": "1UVecDtCcClV-lyn9q9rYzAww9MsxHhoc5aGV6vfziTE",
      "name": "Shortlisted",
      "mimeType": "application/vnd.google-apps.spreadsheet",
      "parents": [
        "1XBvvpk4-RVsXVaRWCBrQUleLVEpsJ-I7"
      ],
      "modifiedTime": "2018-04-08T20:10:09.850Z"
    },
    {
      "id": "1umwmlhSYA3r4u60IhD4HqXjIT_Tmti5eE0lzQS9M2uY",
      "name": "Sample Project Applications",
      "mimeType": "application/vnd.google-apps.spreadsheet",
      "parents": [
        "1XBvvpk4-RVsXVaRWCBrQUleLVEpsJ-I7"
      ],
      "modifiedTime": "2018-04-08T20:05:17.072Z"
    },
    {
      "id": "1yxL2f78PpI3blKmW1Ojr5DlvsMzEPWPpHEbVZao2smE",
      "name": "00_Timeline + SDM Museum BEI",
      "mimeType": "application/vnd.google-apps.spreadsheet",
      "parents": [
        "1U0FeMWxrMo4Yo_KIGCmFhVsVdzBxkqcU"
      ],
      "modifiedTime": "2018-04-07T21:18:01.495Z"
    },
    {
      "id": "1fSAGrUMoFZW_lafGUkdX3BwGgxhboEGDYS0pVHCDNEw",
      "name": "Copy of Sample Project Application",
      "mimeType": "application/vnd.google-apps.form",
      "parents": [
        "1XBvvpk4-RVsXVaRWCBrQUleLVEpsJ-I7"
      ],
      "modifiedTime": "2018-03-24T22:20:20.766Z"
    },
    {
      "id": "1qe2gFNm1TbscQOtenD1u9lnSTSd-zJtmbuXDfOPyL8I",
      "name": "Database",
      "mimeType": "application/vnd.google-apps.spreadsheet",
      "parents": [
        "1KW1B5_XkdFGyOzTxPAPy8o6WVcyUoxxj"
      ],
      "modifiedTime": "2018-03-24T18:39:18.192Z"
    },
    {
      "id": "1A--m3qA_7FhbC4dWWm6iR4PVFXMeY1Gl6HVVxKCIalw",
      "name": "Sample Project Application",
      "mimeType": "application/vnd.google-apps.form",
      "parents": [
        "1XBvvpk4-RVsXVaRWCBrQUleLVEpsJ-I7"
      ],
      "modifiedTime": "2018-03-24T18:33:39.466Z"
    },
    {
      "id": "1U0FeMWxrMo4Yo_KIGCmFhVsVdzBxkqcU",
      "name": "Projects",
      "mimeType": "application/vnd.google-apps.folder",
      "parents": [
        "1SyyN7CzhA_EoDXYxnOnHBz4c5toMzfWH"
      ],
      "modifiedTime": "2018-03-24T18:13:49.365Z"
    },
    {
      "id": "1KW1B5_XkdFGyOzTxPAPy8o6WVcyUoxxj",
      "name": "Freelancers",
      "mimeType": "application/vnd.google-apps.folder",
      "parents": [
        "1SyyN7CzhA_EoDXYxnOnHBz4c5toMzfWH"
      ],
      "modifiedTime": "2018-03-24T15:10:53.559Z"
    },
    {
      "id": "1SyyN7CzhA_EoDXYxnOnHBz4c5toMzfWH",
      "name": "Labtek Indie Shared Folder",
      "mimeType": "application/vnd.google-apps.folder",
      "parents": [
        "0AA32T9b3djAmUk9PVA"
      ],
      "modifiedTime": "2018-03-24T14:28:05.573Z"
    },
    {
      "id": "1R74BuyEhQfzOi3-wF0IwkCNx4NsPdX_-",
      "name": "Ad hoc sheets",
      "mimeType": "application/vnd.google-apps.folder",
      "parents": [
        "1SyyN7CzhA_EoDXYxnOnHBz4c5toMzfWH"
      ],
      "modifiedTime": "2018-03-21T19:47:56.549Z"
    },
    {
      "id": "1OpFz-vbhHFf9k-vF_e-A4MFmeOSfBxFUJLWKwyBtbqQ",
      "name": "Statistical Progress VM Ultrajaya - Sprint 1A",
      "mimeType": "application/vnd.google-apps.spreadsheet",
      "parents": [
        "1AJZnZswAXtmtxjYBjfbKR_bjjJA7Lohz"
      ],
      "modifiedTime": "2017-11-14T01:02:56.555Z"
    }
  ]

  const files = {}
  filesList.map(file => {
    files[file.id] = file
  })

  let rootFolder
  filesList.map(file => {

    const parentId = file.parents[0]
    const parent = files[parentId]

    if(parent){
      if(!parent.children)
        parent.children = []

      parent.children.push(file)
    }
    else
      rootFolder = file
  })

  return rootFolder
}

function createTransferConfigs (
  rootFolder,
  dataConfigs,
  lastModified) {

  const transferConfigs = collectTransferConfigs(
    rootFolder, dataConfigs.folderStructure, dataConfigs
  )

  return transferConfigs
}

function collectTransferConfigs (
  file,
  fileConfig,
  dataConfigs,
  externalData = {},
  transferConfigs = {
    fetch: [],
    send: [],
  }) {

  let dataConfig = fileConfig.dataConfig
  if(dataConfig){

    // collect metadata
    const metadata = dataConfig.metadata
    if(metadata){

      _.forEach(metadata, (val, key) => {
        externalData[val.name] = file[key]
      })
    }

    // replace dataConfig address with the object
    if(typeof dataConfig == 'string')
      dataConfig = _.get(dataConfigs, [
        fileConfig.direction,
        fileConfig.type,
        ...dataConfig.split('.')
      ])

    // TODO somehow merge these?
    // if input create fetchConfig
    if(fileConfig.direction == 'input'){
      transferConfigs.fetch.push({
        type: fileConfig.type,
        id: file.id,
        dataConfig: dataConfig,
        externalData: _.clone(externalData),
      })
    }
    // else if output, create queryConfig
    else if(fileConfig.direction == 'output'){
      transferConfigs.send.push({
        type: fileConfig.type,
        id: file.id,
        name: file.name,
        folder: file.folder,
        dataConfig: dataConfig,
        externalData: _.clone(externalData),
      })
    }
  }

  // if folder AND has children, dig deeper
  if(
    fileConfig.type == 'folder' &&
    fileConfig.children
  ){

    // for each children's fileConfig
    fileConfig.children.map(childConfig => {

      let childFiles = file.children

      // if there are children
      if(childFiles){

        // collect child files that match the fileConfig
        childFiles = findMatches(
          childFiles,
          childConfig.type,
          childConfig.namePattern
        )

        // if file is not found but the direction is output,
        if(
          childFiles.length == 0 &&
          childConfig.direction == 'output'
        ){
        // create object with name and folder // IDEA: so the file will be created later
          childFiles = [{
            name: childConfig.namePattern,
            folder: file.id
          }]
        }

        // for each child file
        if(childFiles.length > 0){
          childFiles.map(childFile => {
            collectTransferConfigs(
              childFile,
              childConfig,
              dataConfigs,
              _.clone(externalData),
              transferConfigs,
            )
          })
        }
      }
    })
  }

  return transferConfigs
}

function findMatches (files, type, pattern) {

  const res = []

  _.forEach(files, file => {

    // if the file type matches
    let fileType
    switch (file.mimeType) {
      case 'application/vnd.google-apps.folder':
        fileType = 'folder'
        break;
      case 'application/vnd.google-apps.spreadsheet':
        fileType = 'spreadsheet'
        break;
      default:
    }
    if(fileType == type){

      // if there's no pattern, add
      if(!pattern){
        res.push(file)
        return true
      }

      // if there's a name pattern
      // if ordinary string, check if equal
      if(pattern instanceof RegExp == false){
        if(file.name == pattern){
          res.push(file)
          // and skip the rest
          return false
        }
      }
      // else if regex
      else
        if(file.name.match(pattern))
          res.push(file)
    }
  })

  return res
}

async function fetchData (
  configs,
  fetchFunctions = {},
  valueProcessor) {

  const tableJsons = {}

  // for each fetchConfig
  for (fetchConfig of configs) {

    // if "table", handle internally
    if(fetchConfig.type == 'folder') {

      // for each table
      _.forEach(
        fetchConfig.dataConfig.tables,
        (tableConfig, tableName) => {

          // create table array
          tableJsons[tableName] = tableJsons[tableName] || []

          const newEntry = {}

          // for each column
          _.forEach(
            tableConfig.columns,
            (columnConfig, columnName) => {
              // fill with external data

              newEntry[columnName] = valueProcessor(
                columnConfig,
                fetchConfig.externalData
              )
            }
          )

          tableJsons[tableName].push(newEntry)
        }
      )
    }
    // else, use fetchFunctions
    else if(fetchFunctions[fetchConfig.type]){
      await fetchFunctions[fetchConfig.type](
        fetchConfig,
        valueProcessor,
        tableJsons
      )
    }
  }

  // for each entry in every table
  // add last_update
  const now = new Date()
  _.forEach(tableJsons, (entries, tableName) => {
    entries.map(entry => {
      entry.last_update = now
    })
  })

  return tableJsons
}

module.exports = {
  getFolderStructure,
  createTransferConfigs,
  fetchData,
}
