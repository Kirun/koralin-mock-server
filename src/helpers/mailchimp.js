var Mailchimp = require('mailchimp-api-v3')
var token = require('../../configs/mailchimp.json').token
var _ = require('lodash')

var mailchimp = new Mailchimp(token)
const baseUrl = 'http://koralin.com/projects/'
const freelancerListId = '4011d8d98e'
const projectInvitationTemplateId = 129081

/**
 * Get basic relevant fields of all campaigns in a simplified format
 * @return {Object} open details
 */
async function getCampaignList(){

  const result = await mailchimp.get({
    path: '/campaigns',
    query: {
      fields: [
        'campaigns.id',
        'campaigns.settings.title',
        // 'campaigns.create_time',
        'campaigns.send_time',
        // 'campaigns.status',
        'campaigns.emails_sent',
        'campaigns.recipients.recipient_count',
      ].join(),
    }
  }).catch((err) => console.log(err))

  return result.campaigns.map(
    (campaign) => {
      return {
        id: campaign.id,
        title: campaign.settings.title,
        send_time: campaign.send_time,
        emails_sent: campaign.emails_sent,
        recipient_count: campaign.recipients.recipient_count,
      }
    }
  )
}

/**
 * Get some details of a campaign
 * @param  {String} campaignId id of the campaign
 * @return {Object} campaign details
 */
async function getCampaignDetails(campaignId){

  const result = await mailchimp.get({
    path: '/campaigns/'+campaignId,
    query: {
      fields: [
        'id',
        'title',
        'send_time',
        'emails_sent',
        'recipients.recipient_count',
      ].join(),
    }
  }).catch((err) => console.log(err))

  result.recipient_count =
    result.recipients.recipient_count

  result.send_time = result.send_time

  delete result.recipients

  return result
}



/**
 * [getOpenDetails description]
 * @param  {String} campaignId id of the campaign
 * @return {Array}  open details
 */
async function getOpenDetails(campaignId){

  const result = await mailchimp.get({
    path: '/reports/'+campaignId+'/open-details',
    query: {
      fields: [
        'members.email_address',
        'members.opens',
      ].join(),
    }
  }).catch((err) => console.log(err))

  const invitation_opens = []

  result.members.map(member => {
    member.opens.map(open => {
      invitation_opens.push({
        campaign: campaignId,
        freelancer_email: member.email_address,
        timestamp: open.timestamp,
      })
    })
  })

  return invitation_opens
}

/**
 * Get basic relevant fields of all campaigns in a simplified format
 * @param  {String} campaignId id of the campaign
 * @return {Array}  click details
 */
async function getClickDetails(campaignId){
  // 757b9c5768

  const path = '/reports/'+campaignId+'/click-details'

  const result = await mailchimp.get({
    path: path,
    query: {
      fields: [
        'urls_clicked.id',
        'urls_clicked.url',
        'urls_clicked.total_clicks',
      ].join(),
    }
  }).catch((err) => console.log(err))

  const clicks = []
  const urls = []

  // for each url
  for (const urlData of result.urls_clicked) {

    // collect url
    urls.push({
      campaign: campaignId,
      url: urlData.url
    })

    // and if it's been clicked, fetch more info
    if(urlData.total_clicks > 0){

      const clickData = await mailchimp.get({
        path: path+'/'+urlData.id+'/members',
        query: {
          fields: [
            'members.email_address',
            'members.clicks',
          ].join(),
        }
      }).catch((err) => console.log(err))

      clickData.members.map(member => {
        clicks.push({
          campaign: campaignId,
          url: urlData.url,
          freelancer_email: member.email_address,
          count: member.clicks,
        })
      })
    }
  }

  return {
    urls,
    clicks
  }
}



/**
 * Create and publish a campaign with a static segment (fixed list of emails) that will be immediately deleted
 * @param  {Function} callback function to be called with campaignId and segmentId when the campaign is created (otw to be sent)
 * @return {void}
 */
function createInstantCampaign(freelancerProject, callback){

  let segmentIds, campaignIds

  // create html version of each role
  const roleHTMLs = {}
  _.forEach(freelancerProject.roles,
    (role, name) => {

      let rolesHTML = ''
      rolesHTML += '<p><strong>'+name+'</strong> '

      _.forEach(role.qualifications, (val, type) => {

        val.map(qualification => {
          rolesHTML += `<span class="qualification ${type}">`
          rolesHTML += qualification
          rolesHTML += '</span>'
        })

      })

      rolesHTML += '<span> <strong>'
      rolesHTML += role.nPositions
      rolesHTML += '</strong> positions open</span></p>'

      roleHTMLs[name] = rolesHTML
    }
  )

  // create segments
  const requests = freelancerProject.segments.map(
    (segment, idx) => {
      return {
        method: 'post',
        path: '/lists/'+freelancerListId+'/segments',
        body: {
          name: 'one use segment ' + Date.now() + ' ' + segment.roles.join(),
          static_segment: segment.emails
        }
      }
    }
  )
  mailchimp.batch(requests)
  // create campaigns
  .then(segments => {

    segmentIds = segments.map(segment => segment.id)

    const requests = freelancerProject.segments.map(
      (segment, idx) => {
        return {
          method: 'post',
          path: '/campaigns',
          body: {
            type: 'regular',
            recipients: {
              listId: freelancerListId,
              segment_opts: {
                saved_segmentId: segmentIds[idx],
              }
            },
            settings: {
              folderId: '3ce4b51359',
              title: `Inv. for ${freelancerProject.name} (${segment.roles.join()})` ,
              subject_line: 'Invitation for ' + freelancerProject.name,
              from_name: 'Labtek Indie',
              reply_to: 'irun.siregar@gmail.com',
              to_name: '*|FNAME|*',
            }
          }
        }
      }
    )

    return mailchimp.batch(requests)
  })
  // set campaign contents
  .then(campaigns => {

    campaignIds = campaigns.map(campaign => campaign.id)
    // callback(campaignIds)
    callback({
      segments: segmentIds,
      campaigns: campaignIds
    })

    const requests = freelancerProject.segments.map(

      (segment, idx) => {

        const url = baseUrl+freelancerProject.url+'/'+segment.url

        return {
          method: 'put',
          path: '/campaigns/'+campaignIds[idx]+'/content',
          body: {
            template: {
              id: projectInvitationTemplateId,
              sections: {

                project_name: freelancerProject.name,

                role_names:
                  segment.roles.map(role => '<strong>'+role+'</strong>')
                  .join(', '),

                startDate: freelancerProject.startDate,
                endDate: freelancerProject.endDate,

                type: freelancerProject.type,
                industry: freelancerProject.industry,
                status: freelancerProject.status,

                description: freelancerProject.description,

                rolesAndQualifications:
                  segment.roles.map(role => roleHTMLs[role]).join(''),
                otherRoles:
                  _.without(
                    _.keys(freelancerProject.roles), ...segment.roles
                  )
                  .map(role => '<strong>'+role+'</strong>')
                  .join(', '),

                button:
                  '<a class="mcnButton " title="Project Details" href="'+url+'" target="_blank">Project Details</a>',
              }
            }
          }
        }
      }
    )

    return mailchimp.batch(requests)
  })
  // send campaigns
  .then(res => {

    const requests = campaignIds.map(
        campaignId => {
          return {
            method: 'post',
            path: '/campaigns/'+campaignId+'/actions/send'
          }
        }
      )

    return mailchimp.batch(requests)
  })
  // delete segments
  // .then(res => {
  //
  //   const requests = segmentIds.map(
  //     segmentId => {
  //       return {
  //         method: 'delete',
  //         path: '/lists/'+freelancerListId+'/segments/'+segmentId
  //       }
  //     }
  //   )
  //
  //   return mailchimp.batch(requests)
  // })
  .catch(err => {
    console.log('ERROR: ')
    console.log(err)
  })
}


module.exports = {
  getCampaignList,
  createInstantCampaign,
  getCampaignDetails,
  getOpenDetails,
  getClickDetails,
}
