const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')
const Op = Sequelize.Op
const _ = require('lodash')

const config = require('../../configs/db.json')
const data = require('../../data')
const dataConfig = require('../../dataConfig')

// create sqlite file if one doesn't exist yet
if(!fs.existsSync(config.storage)){
  const sqlite3 = require('sqlite3').verbose()
  new sqlite3.Database(config.storage)
}

// initialize database

config.logging = false
// config.logging = console.log
config.define = {
  // timestamps: true,
  // underscored: true,
  freezeTableName: true,
}

const sequelize = new Sequelize(
  config.database,
  config.username,
  config.password,
  config
)

// convert schemas to sql

const toSqlDataType = {
  string: 'STRING',
  integer: 'INTEGER',
  float: 'FLOAT',
  bool: 'BOOLEAN',
  date: 'DATE',
  datetime: 'DATE',
}

// set raw queries
// get list of tables
let listQuery
switch (config.dialect) {
  case 'sqlite':
    listQuery = `SELECT name FROM sqlite_master WHERE type='table'`
    break
  default:
    console.log('list tables query not set for this dialect: '+config.dialect)
}

// drop table
let dropQuery
switch (config.dialect) {
  case 'sqlite':
    dropQuery = 'DROP TABLE IF EXISTS '
    break
  default:
    console.log('drop query not set for this dialect: '+config.dialect)
}


const db = {}
//  collect tables from schema
_.forEach(dataConfig.classes, (table, tableName) => {
  db[tableName] = table
})
_.forEach(dataConfig.joins, (table, tableName) => {
  db[tableName] = table
})

async function initModels (restartDB = false){

  return sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
  })
  .then(async res => {

    if(restartDB){
      console.log('------DROP TABLES')

      // get list of tables
      const tables = await sequelize.query(listQuery)

      // and drop them
      const promises = []
      tables.map(tableName => {
        promises.push(
          sequelize.query(dropQuery+tableName)
        )
      })

      return Promise.all(promises)
    }
    else{
      return sequelize.sync()
    }
  })
  .then(res => {

    console.log('------CREATE TABLES')
    _.forEach(db, (table, tableName) => {

      const config = {}
      _.forEach(table.config, (column, columnName) => {
        // convert data types to sql data types
        config[columnName] = _.clone(column)
        config[columnName].type
        = Sequelize[
            toSqlDataType[config[columnName].type]
          ]

        // add primaryKey: true to ids
        if(columnName == 'id')
          config[columnName].primaryKey = true

      })

      db[tableName] = sequelize.define(tableName, config)
    })

    return sequelize.sync()
  })
  // associations
  .then(() => {
    console.log('------CONNECT ASSOCIATIONS')

    _.forEach(dataConfig.classes, (table, tableName) => {

      // add hasMany associations from manyBelongsTo
      if(table.manyBelongsTo){

        table.manyBelongsTo.map(config => {

          db[config.class].hasMany(
            db[tableName],
            {
              foreignKey: config.foreignKey,
              as: config.sourceAlias
            }
          )
          // then add the implied belongsTo association
          db[tableName].belongsTo(
            db[config.class],
            {
              foreignKey: config.foreignKey,
              as: config.targetAlias
            }
          )
        })
      }
      // add hasOne associations
      if(table.hasOne){

        table.hasOne.map(config => {

          db[tableName].hasOne(
            db[config.class],
            {
              foreignKey: config.foreignKey,
              as: config.targetAlias
            }
          )
          // then add the implied reverse
          // belongsTo association
          db[config.class].belongsTo(
            db[tableName],
            {
              foreignKey: config.foreignKey,
              as: config.sourceAlias
            }
          )
        })
      }
    })

    // connect joins (belongsToMany associations)
    _.forEach(dataConfig.joins, (joinSchema, joinName) => {

      _.forEach(joinSchema.joins, (sourceName, sourceAlias) => {
        _.forEach(joinSchema.joins, (targetName, targetAlias) => {

          if(sourceAlias != targetAlias){
            db[sourceName].belongsToMany(db[targetName], {
              as: _.get(
                joinSchema,
                ['aliases', targetAlias],
                targetAlias
              ),
              through: joinName,
              foreignKey: sourceAlias,
              otherKey: targetAlias,
            })
          }
        })
      })
    })

    return sequelize.sync()
  })
  // fill class data (if still simulating using mock data)
  .then(() => {

    if(restartDB){
      console.log('------FILL CLASS')

      _.forEach(data.classes, (tableData, tableName) => {

        tableData.map(rowData => {
          db[tableName].create(rowData)
        })
      })
    }

    return sequelize.sync()
  })
  // fill join data (if still simulating using mock data)
  .then((res) => {
    if(restartDB){
      console.log('------FILL JOINS')
      _.forEach(data.joins, (tableData, tableName) => {
        tableData.map(rowData => {
          db[tableName].create(rowData)
        })
      })
    }

    return sequelize.sync()
  })
  .catch(err => {
    console.error('Database error:', err)
  })
}

async function fetchTables (){

  const tables = {}

  for (var modelName in db) {
    if (db.hasOwnProperty(modelName)) {

      const defConfig = {}

      if(config.timestamps){
        defConfig.columns = [
          'created_at',
          'updated_at',
        ]
        defConfig.config = {
          created_at: {type: 'datetime'},
          updated_at: {type: 'datetime'},
        }
      }

      const tableConfig =
        dataConfig.classes[modelName] ||
        dataConfig.joins[modelName]

      const table = tables[modelName] = {
        config: Object.assign(defConfig, tableConfig)
      }

      const rows =
        await db[modelName].findAll()
        .map(row => row.dataValues)

      table.rows = []

      rows.map(row => {
        table.rows.push(
          table.config.columns.map(
            colName => row[colName]
          )
        )
      })

    }
  }

  return tables
}

async function toSql (tableJsons) {

  // for each tables
  for (var tableName in tableJsons) {
    if (tableJsons.hasOwnProperty(tableName)) {
      // empty it
      await db[tableName].truncate()

      // and add new content
      tableJsons[tableName].map(rowData => {
        db[tableName].create(rowData)
      })

      await sequelize.sync()
    }
  }

  // TODO lATER
  // collect primary keys

  // fetch existing data from primary keys
  // compare new and existing data
  // and split new rows to new and updated
  // and collect old rows with obsolete primary keys

  // for each table
  // delete old (cascade)
  // create new
  // update outdated

}

/**
 * convert to and submit sequelize query,
 * and return the result data and model
 * @param  {Object} query the data query (what class and columns to get)
 * @return {Object}       object containing result data and model
 */
async function fromSql (query) {

  // clone query
  query = _.cloneDeep(query)

  // convert query to sequelize query
  const sqlQuery = sqlQueryFromTableQuery(query)
  // sqlQuery.raw = true
  // remove model from the first level
  delete sqlQuery.model

  const data = await db[query.class].findAll(sqlQuery)
  .catch((err) => console.log(err))

  return data
}

function modelFromTableQuery(
  tableQuery,
  parents = [],
  result = undefined,
  isJoin = false) {

  if(!result)
    result = {
      classColumns: [],
      columns: [],
      config: {},
    }

  const table = !isJoin ? tableQuery.class : tableQuery.table

  // if there are columns to get
  if(
    tableQuery.columns != undefined &&
    tableQuery.columns.length > 0
  ){
    // add class column
    result.classColumns.push([
      table,
      tableQuery.columns.length
    ])

    // add column column (+ parents) and config
    tableQuery.columns.map(column => {

      const columnName =
        parents.length == 0 ?
        column :
        parents.join('.')+'.'+column

      result.columns.push(columnName)

      result.config[columnName] = {
        type: dataConfig
          [!isJoin ? 'classes' : 'joins']
          [table]
          .config[column].type,
        alias: column
      }
    })
  }

  // if there's an include, dig deeper
  if(tableQuery.include){
    tableQuery.include.map(include => {
      modelFromTableQuery(
        include,
        parents.concat([include.as]),
        result
      )
    })
  }

  // if there's a join, dig into the join table
  if(tableQuery.join){
    modelFromTableQuery(
      tableQuery.join,
      parents.concat([tableQuery.join.table]),
      result,
      true
    )
  }

  return result
}

function sqlQueryFromTableQuery(tableQuery, isJoin = false) {

  const table = !isJoin ? tableQuery.class : tableQuery.table

  const result = {}

  // add attributes
  result.attributes = tableQuery.columns

  // add where
  if(tableQuery.where)
    result.where = tableQuery.where

  if(tableQuery.class)
    result.model = db[tableQuery.class]

  // if not a join, add others
  if(!isJoin){
    result.as = tableQuery.as

    // if there's an include, dig deeper
    if(tableQuery.include){

      result.include = tableQuery.include.map(include => {
        return sqlQueryFromTableQuery(include)
      })
    }

    // if there's a join, dig into the join table
    if(tableQuery.join){
      result.through = sqlQueryFromTableQuery(
        tableQuery.join,
        true
      )

      // if there's a where in the through,
      // add id != null to the class
      // because otherwise non-matching entries
      // will still be fetched, only with the id = null
      if(result.through.where){
        if(!result.where)
          result.where = {}
        result.where.id = {[Op.not]: null}
      }
    }
  }

  return result
}

module.exports = {
  initModels,
  fetchTables,
  toSql,
  modelFromTableQuery,
  fromSql,
  db, sequelize
}
