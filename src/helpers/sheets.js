const gapiHelper = require('./gapi')
const {google} = require('googleapis')
const drive = google.drive('v3')
const sheets = google.sheets('v4')
const _ = require('lodash')
const toL = require('number-to-letter')

const uid = require('uid')

// letters to numbers util
const LETTERS =
  'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  .split('')

function toN (letters) {
  let res = 0
  const d = letters.length
  letters.toUpperCase().split('').map((L, p) => {
    res += (LETTERS.indexOf(L)+1) * Math.pow(26,d-p-1)
  })
  return res-1
}


// conversion table from schema datatype to sheets data type
const dataTypeToValue = {
  string: 'stringValue',
  float: 'numberValue',
  integer: 'numberValue',
  bool: 'boolValue',
  date: 'numberValue',
  datetime: 'numberValue',
}

// conversion from sheet's string values
// to actual value type
function toValueType (val, type) {
  switch (type) {
    case 'bool':
      val = val == 'TRUE' ? true : false
      break;
    case 'integer':
      val = parseInt(val)
      break;
    case 'number':
      val = Number(val)
      break;
    default:
  }
  return val
}




/**
 * create gsheets request for update rows (value and format) based on a data model
 * @param  {Object} model        table model, see data/schema.js
 * @param  {Object} valueSets    key, value pair (keys no inside the model will be ignored)
 * @param  {Number} [sheetId=0]  sheet id (not spreadsheetId)
 * @param  {Number} [rowIndex=1] starting row
 * @return {Object}              gsheet request
 */
function updateRowsReq (model, valueSets, sheetId = 0, rowIndex = 1) {

  const rows = []

  const request = {
    updateCells: {
      start: {
        sheetId,
        rowIndex,
        columnIndex: 0
      },
      // for each row
      rows: valueSets.map(valueSet => {

        // for each model's column
        return  {
          values: model.columns.map(column => {

            // if data for it exists, create request
            let columnVal = valueSet[column]
            if(columnVal != undefined){

              const config = model.config[column]

              // if date / datetime, convert to excel date format
              if(
                config.type == 'date' ||
                config.type == 'datetime'
              ){
                // shift starting date
                columnVal = Date.parse(columnVal)+2209161600000
                // convert from ms to day
                columnVal = columnVal/(1000*60*60*24)
                // if date, round to day
                if(config.type == 'date')
                  columnVal = Math.round(columnVal)
              }

              // if bool, convert possible integers to bool
              else if(config.type == 'bool'){
                if(columnVal == 1)
                  columnVal = true
                else if(columnVal == 0)
                  columnVal = false
              }

              // determine data type, and then fill
              const val = {}
              val[dataTypeToValue[config.type]]
              = columnVal

              const valueSet = {
                userEnteredValue: val
              }

              // if date / datetime, add formatting
              if(config.type == 'date'){
                valueSet.userEnteredFormat = {
                  numberFormat: {
                    type: 'DATE',
                    pattern: 'yyy-mm-dd',
                  }
                }
              }
              else if(config.type == 'datetime'){
                valueSet.userEnteredFormat = {
                  numberFormat: {
                    type: 'DATE',
                    pattern: 'yyy-mm-dd hh+:mm:ss',
                  }
                }
              }

              return valueSet
            }
            // else leave empty
            else return {}
          })
        }
      }),
      fields: 'userEnteredValue,userEnteredFormat.numberFormat'
    }
  }

  return request
}

/**
 * create and fill a spreadsheet file in a folder based on a model (asyinc)
 * @param  {String} id     the spreadsheet's id
 * @param  {Object} model  table model, see data/schema.js
 * @param  {Object} data   the data object
 * @return {String}        the id of the newly created file (Promise)
 */
async function fillSpreadsheet (id, model, data) {

  await gapiHelper.call(sheets.spreadsheets.batchUpdate, {
    spreadsheetId: id,
    resource: {
      requests: updateRowsReq(
        model, data,
        0,
        model.classColumns ? 2 : 1
      )
    }
  })
  .catch((err) => console.log(err))
}

/**
 * create request to create empty spreadsheet file with empty sheets
 * @param  {String} title               the spreadsheet's title
 * @param  {Array}  [sheets=["Sheet1"]] name of the sheets inside the spreadsheet file
 * @return {Object}                     gsheet request
 */
function createSpreadsheetReq (title, sheets = ['Sheet1']) {
  return {
    resource: {
      properties: {
        title
      },
      sheets: sheets.map((sheet, idx) => {
        return {
          properties: {
            sheetId: idx,
            title: sheet
          }
        }
      })
    }
  }
}

/**
 * create request to create a new table and fill the header based on a model
 * @param  {String} title the spreadsheet's title
 * @param  {Object} model table model, see data/schema.js
 * @return {Object}       gsheet request
 */
function createSpreadsheetAndFillHeaderReq (title, model) {
  const request = createSpreadsheetReq(title, ['Data'])
  const sheet = request.resource.sheets[0]

  const rowData = []

  // if two rowed header sheet,
  // create the first merged header first
  if(model.classColumns){

    const values = []
    sheet.merges = []
    let colIdx = 0

    model.classColumns.map(columnGroup => {
      values.push({
        userEnteredValue: {
          stringValue: columnGroup[0]
        },
        userEnteredFormat: {
          textFormat: {
            bold: true
          }
        }
      })
      // add extra empty cells
      for (var i = 1; i < columnGroup[1]; i++) {
        values.push({})
      }

      // and merge the columns
      if(columnGroup[1] > 1){
        sheet.merges.push({
          sheetId: 0,
          startRowIndex: 0,
          endRowIndex: 1,
          startColumnIndex: colIdx,
          endColumnIndex: colIdx + columnGroup[1],
        })
      }

      colIdx += columnGroup[1]
    })

    rowData[0] = ({values})
  }

  sheet.properties.gridProperties = {
    columnCount: model.columns.length,
    frozenRowCount: model.classColumns ? 2 : 1
  }

  const values = []
  model.columns.map(column => {
    // use alias if supplied
    column = model.config[column].alias || column
    values.push({
      userEnteredValue: {
        stringValue: column
      },
      userEnteredFormat: {
        textFormat: {
          bold: true
        }
      }
    })
  })
  rowData.push({values})

  sheet.data = {
    startRow: 0,
    startColumn: 0,
    rowData
  }

  return request
}


/**
 * create and fill a spreadsheet file in a folder based on a model (async)
 * @param  {Object} spreadsheet  the spreadsheet's details
 * @param  {String} spreadsheet.title  the spreadsheet's title
 * @param  {Object} spreadsheet.model  table model, see data/schema.js
 * @param  {String} spreadsheet.folder the folder id
 * @param  {Object} spreadsheet.data   the data object
 * @return {String} the id of the newly created file (Promise)
 */
async function createAndFillSpreadsheet ({title, model, folder, data}) {

  const createReq = createSpreadsheetAndFillHeaderReq(
    title, model
  )

  // create new table, based on the model
  const res = await gapiHelper.call(sheets.spreadsheets.create, createReq)
  .catch((err) => console.log(err))
  const spreadsheetId = res.spreadsheetId

  // move sheet file to the folder
  await gapiHelper.call(
    drive.files.update,
    {
      fileId: spreadsheetId,
      addParents: folder
    }
  )
  .catch((err) => console.log(err))

  // fill with data
  if(data) {

    await fillSpreadsheet(
      spreadsheetId,
      model,
      data,
    )
  }

  return spreadsheetId
}

async function readSheetToJson (
  fetchConfig,
  valueProcessor,
  tableJsons = {}) {

  const {
    id: spreadsheetId,
    dataConfig,
    externalData,
  } = fetchConfig

  let sheetConfigs =
    _.clone(dataConfig.includedSheetsByTitle) || {}

  // collect sheet infos
  const sheetsInfoRes = await gapiHelper.call(
    sheets.spreadsheets.get,
    {
      spreadsheetId,
      fields: [
        'sheets.properties.sheetId',
        'sheets.properties.title',
        'sheets.merges',
        'sheets.properties.gridProperties.columnCount'
      ].join()
    }
  )
  .catch((err) => console.log(err))

  const sheetsInfo = sheetsInfoRes.sheets.map(
    info => {
      return {
        id: info.properties.sheetId,
        title: info.properties.title,
        merges: info.merges,
        columnCount: info.properties.gridProperties.columnCount,
      }
    }
  )

  // if NOT using inclusions
  if(
    !dataConfig.includedSheetsByTitle &&
    !dataConfig.includedSheetsByIndex
  ){
    // collect all, and use defaultModel
    sheetsInfo.map(sheetInfo => {
      const title = sheetInfo.title
      sheetConfigs[title] =
        dataConfig.defaultModel
    })
  }
  // else if including by index, collect
  else if(dataConfig.includedSheetsByIndex){
    _.forEach(dataConfig.includedSheetsByIndex,
      (model, idx) => {
        idx = parseInt(idx)
        // if negative, count from the end
        if(idx < 0) idx += sheetsInfo.length
        const title = sheetsInfo[idx].title
        sheetConfigs[title] = model
      }
    )
  }


  // then remove exceptions
  if(dataConfig.excludedSheetsByTitle){
    dataConfig.excludedSheetsByTitle.map(title => {
      delete sheetConfigs[title]
    })
  }
  if(dataConfig.excludedSheetsByIndex){
    dataConfig.excludedSheetsByIndex.map(idx => {
      idx = parseInt(idx)
      // if negative, count from the end
      if(idx < 0) idx += sheetsInfo.length
      const title = sheetsInfo[idx].title
      delete sheetConfigs[title]
    })
  }

  // convert sheetsInfo to object
  const sheetsInfoByTitle = {}
  sheetsInfo.map(sheetInfo => {
    sheetsInfoByTitle[sheetInfo.title] =
      sheetInfo
  })

  // then fetch from each sheet
  for (var sheetTitle in sheetConfigs) {

    if (sheetConfigs.hasOwnProperty(sheetTitle)) {

      const tables = await fetchSheetToJson({
        spreadsheetId,
        sheetInfo: sheetsInfoByTitle[sheetTitle],
        sheetConfig: sheetConfigs[sheetTitle],
        externalData,
        valueProcessor
      })
      // merge with tableJsons
      _.forEach(tables, (jsons, tableName) => {
        tableJsons[tableName] = tableJsons[tableName] || []
        tableJsons[tableName].push(...jsons)
      })
    }
  }

  return tableJsons
}

async function fetchSheetToJson ({
  spreadsheetId,
  sheetInfo,
  sheetConfig,
  externalData={},
  valueProcessor}){

  let col0 = sheetConfig.startCells[0]
  let colN = toL(sheetInfo.columnCount-1)
  const row0 = sheetConfig.startCells[1]-1
  const rowN = 9-1

  const range = `'${sheetInfo.title}'!`
    + col0 + (row0+1)
    // + ':' + colN
    + ':' + colN + (rowN+1)

  col0 = toN(col0)
  colN = toN(colN)


  // fetch the spreadsheet values
  const contentRes = await gapiHelper.call(
    sheets.spreadsheets.values.get,
    {
      spreadsheetId,
      range,
      fields: 'values'
    }
  )
  .catch((err) => console.log(err))
  let contents = contentRes.values

  // fill values of merged columns
  if(sheetInfo.merges){

    sheetInfo.merges.map(merge => {

      // if in range y
      if(
        merge.startRowIndex-row0 >= 0 &&
        merge.startRowIndex-row0 < contents.length
      ){

        const source =
          contents[merge.startRowIndex-row0][merge.startColumnIndex-col0]

        // if source is in range
        if(source){

          // for each vertical
          for (
            let y = merge.startRowIndex - row0;
            y < contents.length &&
            y < merge.endRowIndex - row0;
            y++
          ) {

            // if there's a vertical merge
            if(merge.endRowIndex - merge.startRowIndex > 1)
              contents[y][merge.startColumnIndex-col0] = source

            // for each horizontal
            for (
              let x = merge.startColumnIndex - col0;
              x < merge.endColumnIndex - col0;
              x++
            ) {
              // if there's a horizontal merge
              if(merge.endColumnIndex - merge.startColumnIndex > 1)
                contents[y][x] = source
            }
          }
        }
      }
    })
  }

  const headers = contents.slice(0, sheetConfig.headerRows)
  const values = contents.slice(sheetConfig.headerRows)

  // if there's a timeline, collect the dates
  const dates = []
  if(sheetConfig.timelineStart != undefined){
    values.map((row, y) => {

      dates[y] = []

      // for every column in timeline
      if(row.length > model.timelineStart){
        for (
          let x = model.timelineStart-1;
          x < row.length;
          x++
        ) {
          // if cell is not empty
          if(row[x].length > 0)

            // get date from timeline
            dates[y].push(
              headers[2][x]
              + ' ' + headers[1][x]
              + ', ' + headers[0][x]
            )
        }
      }
    })
  }

  // integrate sheetWideCells with externalData
  const sheetWideData = _.clone(externalData)
  if(sheetConfig.sheetWideCells){
    _.forEach(sheetConfig.sheetWideCells, (coords, columnName) => {
      // if single cell
      if(coords.length == 2)
        sheetWideData[columnName] =
          contents[coords[1]-1][toN(coords[0])]
      // if range, collect the values
      else{
        sheetWideData[columnName] = []
        for (var y = coords[1]-1; y <= coords[3]-1; y++) {
          for (var x = toN(coords[0]); x <= toN(coords[2]); x++) {
            if(contents[y][x] != undefined)
              sheetWideData[columnName].push(contents[y][x])
          }
        }
      }
    })
  }

  // convert column object to array
  // so it can be easily referred by rows
  const columns = []
  _.forEach(sheetConfig.columns, (name, col) => {
    columns[toN(col)] = name
  })

  values.map((row, y) => {

    // add sheetWideCells
    values[y] = _.cloneDeep(sheetWideData)

    row.map((val, x) => {

      // if the column is used
      if(columns[x]){

        // if a csv, split the value, and convert
        if(columns[x].csv)
          val = val.split(',').map(
            v => toValueType(v.trim(), columns[x].type)
          )
        // else, simply convert value
        else
          val = toValueType(val, columns[x].type)

        // name columns and convert to data types
        values[y][columns[x].name] = val
      }
    })
  })

  const tables = {}
  _.forEach(sheetConfig.tables, (tableConfig, tableName) => {
    tables[tableName] = []
  })

  // split to tables and columns
  _.forEach(sheetConfig.tables, (tableConfig, tableName) => {


    let prevEntry = {}

    // check if not using value rows
    const rowColumns =
      _.pull(
        _.keys(tableConfig.columns).map(
          columnName =>
            tableConfig.columns[columnName].source
        ),
        ..._.keys(sheetWideData),
      )

    let usedValues

    // if not, use the values
    if(rowColumns.length > 0)
      usedValues = values

    // if yes, don't use the values,
    // but the special values instead
    else
      usedValues = [sheetWideData]

    // for each row
    usedValues.map((row, y) => {


      // check for conditions
      if(tableConfig.where){

        let fails = false

        _.forEach(
          tableConfig.where,
          (whereConfig, columnName) => {

            // TODO convert dates first so they can be compared
            // add not, less than, more than, and between

            // if doesn't match
            if(row[columnName] != whereConfig.eq){
              // set as fail, and skip the rest
              fails = true
              return false
            }
          }
        )

        // if failed, nullify prevEntry and skip
        if(fails){
          prevEntry = {}
          return true
        }
      }

      const newRow = {}
      let sameEntry = true
      const multirows = []

      // for each non-timeline column
      _.forEach(tableConfig.columns, (columnConfig, columnName) => {

        newRow[columnName] = valueProcessor(
          columnConfig,
          row
        )

        // detect if same entry as prev
        if(sameEntry)
          sameEntry =
            newRow[columnName] == prevEntry[columnName]

        // collect multirows
        if(columnConfig.multirow)
          multirows.push(columnName)
      })

      // for each timeline column
      _.forEach(tableConfig.timelines, (columnConfig, columnName) => {

        if(columnConfig.point == 'start')
          newRow[columnName] = dates[y][0]
        else
          newRow[columnName] = _.last(dates[y])

        // if still the same entry, combine
        if(sameEntry){
          if(
            (
              columnConfig.point == 'start' &&
              Date.parse(prevEntry[columnName]) >
              Date.parse(newRow[columnName])
            ) ||
            (
              columnConfig.point == 'end' &&
              Date.parse(prevEntry[columnName]) <
              Date.parse(newRow[columnName])
            )
          )
            prevEntry[columnName] = newRow[columnName]
        }

      })

      // if different from prev row
      if(!sameEntry){
        // save current for use by next row
        prevEntry = newRow

        // if there are no multirows, simply add
        if(multirows.length == 0)
          tables[tableName].push(newRow)

        // if there are multirows, create new row for each value
        else{

          let splitRows = [newRow]
          // for each csv
          _.forEach(multirows, multirowName => {

            const newSplitRows = []

            // for each row that has been split
            splitRows.map(row => {
              // if value is undefined, simply add
              if(!row[multirowName])
                newSplitRows.push(row)
              // else, clone row for every value
              else {

                let vals = row[multirowName]

                // if the multirow is a csv, split
                if(tableConfig.columns[multirowName].multirow == 'csv')
                  vals = vals.split(',')

                vals.map(val => {
                  const clonedRow = _.clone(row)
                  clonedRow[multirowName] = val
                  newSplitRows.push(clonedRow)
                })
              }
            })
            // save the new splt rows to be further split
            // by subsequent multirows
            splitRows = newSplitRows
          })

          tables[tableName].push(...splitRows)
        }
      }
    })
  })

  return tables
}

module.exports = {
  updateRowsReq,
  createSpreadsheetReq,
  createSpreadsheetAndFillHeaderReq,
  fillSpreadsheet,
  createAndFillSpreadsheet,

  readSheetToJson,
}
