var {google} = require('googleapis')
var key = require('../../configs/gapi-key.json')

const jwtClient = new google.auth.JWT(
  key.client_email,
  null,
  key.private_key,
  [
    'https://www.googleapis.com/auth/drive',
    // 'https://www.googleapis.com/auth/drive.file',
    'https://www.googleapis.com/auth/spreadsheets'
  ],
  null
);

function authorize () {
  return jwtClient.authorize()
}

function call (func, payload={}) {
  payload.auth = jwtClient
  return new Promise(
    (resolve, reject) => {
      func(
        payload,
        (err, resp) => {
          if(err)
            reject(err)
          else
            resolve(resp.data)
        }
      )
    }
  )
}


module.exports = {
  authorize,
  call
}
