const jobBriefInfos = {
  jobBriefs: [
    {
      id: 'job_2',
      name: 'Job 2',
      status: 'draft',
      shortDescription: 'description of job 2',
      startDate: 1523466000000,
      roles: {
        'job_2|role_1': {
          id: 'job_2|role_1',
          name: 'UI Designer',
          needed: 2,
          _order: 0,
          nPotential: 99,
          qualifications: {
            ui_design: {
              id: 'ui_design',
              _order: 0,
              level: 1
            }
          }
        },
        'job_2|role_2': {
          id: 'job_2|role_2',
          name: 'UX Designer',
          needed: 2,
          _order: 1,
          nPotential: 99,
          qualifications: {
            ux_design: {
              id: 'ux_design',
              _order: 0,
              level: 2
            }
          }
        }
      },
    }
  ],
  qualifications: [
    {
      id: 'c#',
      name: 'C#',
      category: 'programming'
    },
    {
      id: 'ui_design',
      name: 'UI Design',
      category: 'art & design'
    },
  ]
}
const jobBriefDetails = {
  jobBriefs: [
    {
      id: 'job_2',
      name: 'Job 2',
      description: '<h2>description of <b>job 2</b></h2>',
      status: 'draft',
      startDate: 1523466000000,
      type: 'sprint',
      updatedAt: 1525263438036,
      publishedAt: null,
      endDate: 1525366800000,
      images: [
        {
          url: 'j2i1.png',
          createdAt: '2018-05-02T12:17:18.035Z',
          updatedAt: '2018-05-02T12:17:18.035Z'
        },
        {
          url: 'j2i2.png',
          createdAt: '2018-05-02T12:17:18.035Z',
          updatedAt: '2018-05-02T12:17:18.035Z'
        },
        {
          url: 'j2p1.pdf',
          createdAt: '2018-05-02T12:17:18.035Z',
          updatedAt: '2018-05-02T12:17:18.035Z'
        }
      ],
      roles: {
        'job_2|role_1': {
          id: 'job_2|role_1',
          name: 'UI Designer',
          needed: 2,
          description: '<h2>The job is <b>designing UI</b></h2>, <p>blablabla</p>',
          _order: 0,
          nPotential: 99,
          qualifications: {
            ui_design: {
              id: 'ui_design',
              _order: 0,
              level: 1
            }
          }
        },
        'job_2|role_2': {
          id: 'job_2|role_2',
          name: 'UX Designer',
          needed: 2,
          description: '<h2>The job is <b>designing UX</b></h2>, <p>blablabla</p>',
          _order: 1,
          nPotential: 99,
          qualifications: {
            ux_design: {
              id: 'ux_design',
              _order: 0,
              level: 2
            }
          }
        }
      }
    },
  ],
  qualifications: [
    {
      id: 'c#',
      name: 'C#',
      category: 'programming'
    },
    {
      id: 'ui_design',
      name: 'UI Design',
      category: 'art & design'
    },
  ]
}

const recruitmentInfos = {
  recruitments: [
    {
      id: 'job_2',
      name: 'Job 2',
      startDate: 1523466000000,
      roles: {
        'job_2|role_1': {
          id: 'job_2|role_1',
          name: 'UI Designer',
          needed: 2,
          _order: 0,
          nPotential: 99,
          qualifications: {
            ui_design: {
              id: 'ui_design',
              _order: 0,
              level: 1
            }
          },
          applicants: {
            erdy: {
              id: 'erdy',
              _order: 0,
              shortlisted: true,
              removed: false,
              createdAt: null,
              signed: true
            },
            erwin: {
              id: 'erwin',
              _order: 1,
              shortlisted: true,
              removed: false,
              createdAt: null
            },
          }
        },
        'job_2|role_2': {
          id: 'job_2|role_2',
          name: 'UX Designer',
          needed: 2,
          _order: 1,
          nPotential: 99,
          qualifications: {
            ux_design: {
              id: 'ux_design',
              _order: 0,
              level: 2
            }
          },
          applicants: {
            erdy: {
              id: 'erdy',
              _order: 0,
              shortlisted: true,
              removed: false,
              createdAt: null
            },
            erwin: {
              id: 'erwin',
              _order: 1,
              shortlisted: false,
              removed: false,
              createdAt: null
            },
          }
        }
      }
    },
  ],
  qualifications: [
    {
      id: 'c#',
      name: 'C#',
      category: 'programming'
    },
    {
      id: 'javascript',
      name: 'Javascript',
      category: 'programming'
    },
  ],
  freelancers: [
    {
      id: 'erdy',
      qualifications: {
        ui_design: {
          id: 'ui_design',
          _order: 0,
          claimed: 1,
          confirmed: 1
        },
        ux_design: {
          id: 'ux_design',
          _order: 1,
          claimed: 2,
          confirmed: 2
        }
      }
    },
  ]
}
const recruitmentDetails = {
  recruitments: [
    {
      id: 'job_2',
      name: 'Job 2',
      startDate: 1523466000000,
      roles: {
        'job_2|role_2': {
          id: 'job_2|role_2',
          name: 'UX Designer',
          needed: 2,
          description: '<h2>The job is <b>designing UX</b></h2>, <p>blablabla</p>',
          _order: 1,
          nPotential: 99,
          qualifications: {
            ux_design: {
              id: 'ux_design',
              _order: 0,
              level: 2
            }
          },
          applicants: {
            erdy: {
              id: 'erdy',
              name: 'erdy',
              _order: 0,
              shortlisted: true,
              removed: false,
              signed: true,
              createdAt: 1525263440974
            },
            erwin: {
              id: 'erwin',
              name: 'erwin',
              _order: 1,
              shortlisted: false,
              removed: false,
              createdAt: 1525263440974
            },
          }
        }
      },
      shortDescription: 'description of job 2'
    },
  ],
  qualifications: [
    {
      id: 'c#',
      name: 'C#',
      category: 'programming'
    },
    {
      id: 'javascript',
      name: 'Javascript',
      category: 'programming'
    },
  ],
  freelancers: [
    {
      id: 'erdy',
      qualifications: {
        ui_design: {
          id: 'ui_design',
          _order: 0,
          claimed: 1,
          confirmed: 1
        },
        ux_design: {
          id: 'ux_design',
          _order: 1,
          claimed: 2,
          confirmed: 2
        }
      },
      portfolios: [
        {
          url: 'erdy_CV.doc',
          online: false,
          type: 'doc'
        },
        {
          url: 'erdy_portfolio.pdf',
          online: false,
          type: 'pdf'
        },
        {
          url: 'linkedin.com/in/erdy',
          online: true,
          type: 'linkedin'
        }
      ]
    },
  ]
}

const freelancerInfos = {
  freelancers: {
    erdy: {
      id: 'erdy',
      name: 'erdy',
      email: 'erdy@gmail.com',
      updatedAt: 1525263438032,
      qualifications: {
        ui_design: {
          id: 'ui_design',
          _order: 0,
          claimed: 1,
          confirmed: 1
        },
        ux_design: {
          id: 'ux_design',
          _order: 1,
          claimed: 2,
          confirmed: 2
        }
      }
    },
  },
  qualifications: [
    {
      id: 'javascript',
      name: 'Javascript',
      category: 'programming'
    },
    {
      id: 'ui_design',
      name: 'UI Design',
      category: 'art & design'
    },
  ]
}
const freelancerDetails = {
  freelancers: {
    erdy: {
      id: 'erdy',
      name: 'erdy',
      email: 'erdy@gmail.com',
      updatedAt: 1525263438032,
      phone: '0876 6125 8672',
      domicile: 'Bandung',
      birthDate: 725907600000,
      portfolios: [
        {
          url: 'erdy_CV.doc',
          online: false,
          type: 'doc',
          createdAt: 1525263438038,
          updatedAt: 1525263438038
        },
        {
          url: 'erdy_portfolio.pdf',
          online: false,
          type: 'pdf',
          createdAt: 1525263438038,
          updatedAt: 1525263438038
        },
      ],
      qualifications: {
        ui_design: {
          id: 'ui_design',
          _order: 0,
          claimed: 1,
          confirmed: 1,
          supportRequest: false,
          updatedByFreelancerAt: 1521738000000,
          lastCheckedAt: 1521738000000
        },
        ux_design: {
          id: 'ux_design',
          _order: 1,
          claimed: 2,
          confirmed: 2,
          supportRequest: false,
          updatedByFreelancerAt: 1521738000000,
          lastCheckedAt: 1521738000000
        }
      }
    },
    thofhans: {
      id: 'thofhans',
      name: 'thofhans',
      email: 'thofhans@gmail.com',
      updatedAt: 1525263438033,
      phone: '0876 6125 8672',
      domicile: 'Bandung',
      birthDate: 601318800000,
      portfolio: [],
      qualifications: {
        ui_design: {
          id: 'ui_design',
          _order: 0,
          claimed: 1,
          confirmed: 0,
          supportRequest: false,
          updatedByFreelancerAt: 1521738000000,
          lastCheckedAt: 1521738000000
        },
        ux_design: {
          id: 'ux_design',
          _order: 1,
          claimed: 2,
          confirmed: 2,
          supportRequest: false,
          updatedByFreelancerAt: 1521738000000,
          lastCheckedAt: 1521738000000
        }
      }
    },
  },
  qualifications: [
    {
      id: 'javascript',
      name: 'Javascript',
      category: 'programming'
    },
    {
      id: 'ui_design',
      name: 'UI Design',
      category: 'art & design'
    },
  ]
}
