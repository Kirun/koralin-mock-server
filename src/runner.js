const sequelizeHelper = require('./src/helpers/sequelize')
const output = require('./dataConfig').output

async function run () {

  await sequelizeHelper.initModels()

  const data = await sequelizeHelper.fromSql(
    // output.jobBriefs()
    // output.jobRole()
    // output.jobBriefDetails('job_2')
    // output.recruitments()
    // output.recruitmentDetails('job_2')
    // output.freelancerRecruitments(['erdy','thofhans'])
    // output.freelancers()
    // output.freelancerDetails('erdy')
  )
  .catch((err) => console.log(err))

  console.log(JSON.stringify(data, undefined, 2))
}

run()
.catch((err) => console.log(err))
