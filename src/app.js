const app = require('express')()
const http = require('http').Server(app)
const io = require('socket.io')(http)
const _ = require('lodash')

const sequelizeHelper = require('./helpers/sequelize')
const mailchimpHelper = require('./helpers/mailchimp')
const sheetsHelper = require('./helpers/sheets')
const driveHelper = require('./helpers/drive')
const gapiHelper = require('./helpers/gapi')

const {google} = require('googleapis')
const drive = google.drive('v3')
const sheets = google.sheets('v4')

const ids = require('../configs/driveIds')
const dataConfig = require('../dataConfig')


async function start () {

  await sequelizeHelper.initModels(true)
    .catch((err) => console.log(err))

  // TEMP get and send all qualification infos
  const qualifications = await dataRequests.qualificationInfos()
    .catch((err) => console.log(err))

  // const data = await dataRequests.jobBriefInfos()
  // const data = await dataRequests.recruitmentDetails('job_2p')
  //   .catch((err) => console.log(err))
  // console.log(JSON.stringify(data, undefined, 2))

  // Whenever someone connects this gets executed
  io.on('connection', (socket) => {
    console.log('A user connected')

    //Whenever someone disconnects this piece of code executed
    socket.on('disconnect', () => {
      console.log('A user disconnected')
    })

    // TEMP send all qualifications
    socket.emit('sendData', {
      request: 'qualificationInfos',
      data: qualifications
    })

    // data request
    socket.on('fetchData', async (payload) => {

      const response = {
        request: payload.request,
        ids: payload.ids,
      }

      response.data = await dataRequests[payload.request](payload.ids)
        .catch((err) => console.log(err))

      socket.emit('sendData', response)
    })

    // data storing
    socket.on('storeData', async (payload, callback) => {

      let id = payload.groupId
      if (!id) {
        id = payload.data ? payload.id : undefined
      }

      const response = {
        request: payload.request,
        // in case in a group, use the supplied group id
        id
      }

      response.status = await dataStorings[payload.request](
        payload.data, payload.groupId, payload.deleteId
      )
        .catch((err) => {
          console.log(err)
          return false
        })

      callback(response)
    })
  })

  http.listen(3000, () => {
    console.log('listening on *:3000')
  })
}

start()
.catch((err) => console.log(err))

/**
 * converts from sequelize result to plain JSON
 * by stringifying it and reparsing
 * @param  {Array} res the sequelize result
 * @return {Array}     the plain JSON version
 */
function sequelizeToJSON (res) {
  res = JSON.stringify(res)
  return JSON.parse(res)
}
/**
 * converts an array with ids
 * to an object where each member
 * has "_order" value
 * @param  {Array}  array  the source array
 * @param  {String} idKey  the key to be used as id
 * @return {Object}       the generated object
 */
function arrayToOrderedObject(array, idKey = 'id') {
  const object = {}
  _.forEach(array, (member, idx) => {
    object[member[idKey]] = member
    object[member[idKey]]._order = idx
  })
  return object
}

/**
 * shortens a text and removes html tags
 * @param  {String} text [description]
 * @return {String}      [description]
 */
function shorten (text, maxLength = 200) {
  return text
    .replace(/<\/?[^>]+(>|$)/g, '')
    .substring(0, maxLength)
}

// JOB BRIEF
/**
 * processes jobBrief infos, and groups drafts & published if needed
 * @param  {[type]} jobBriefs [description]
 * @param  {[type]} shortenDescription [description]
 * @return {[type]}           [description]
 */
async function processJobBriefInfos (jobBriefs, shortenDescription) {
  let qualificationIds = []
  let qualifications = []

  _.forEach(jobBriefs, (jobBrief, idx) => {

    jobBrief.startDate =
      Date.parse(jobBrief.startDate)

    jobBrief.roles =
      arrayToOrderedObject(jobBrief.jobRole)

    _.forEach(jobBrief.roles, role => {

      const newRole = _.clone(role)
      newRole.nPotential = 99

      newRole.qualifications =
        arrayToOrderedObject(role.qualification)
      delete newRole.qualification

      // move level from jobRoleQualification
      _.forEach(newRole.qualifications, qualification => {
        qualification.level =
          qualification.jobRoleQualification.level
        delete qualification.jobRoleQualification
      })

      // collect qualifications
      qualifications = _.union(
        qualifications,
        _.keys(newRole.qualifications)
      )

      jobBrief.roles[role.id] = newRole
    })

    delete jobBrief.jobRole
  })

  const jobBriefDictionary = {}
  jobBriefs.map(jobBrief => {
    jobBriefDictionary[jobBrief.id] = jobBrief
  })

  const draftIds = _.keys(jobBriefDictionary)

  jobBriefs = []

  _.forEach(jobBriefDictionary, jobBrief => {
    // shorten description
    if (shortenDescription) {
      jobBrief.shortDescription =
        shorten(jobBrief.description)
      delete jobBrief.description
    }

    // if published
    if (jobBrief.status === 'published') {

      _.pull(draftIds, jobBrief.id)

      // create new grouping object
      const group = {
        id: jobBrief.id,
        published: jobBrief
      }

      // group with draft (as saved)
      if (jobBrief.draft) {

        // extract draft
        jobBrief.draftId = jobBrief.draft[0]
          ? jobBrief.draft[0].id
          : undefined

        delete jobBrief.draft

        if (jobBrief.draftId) {

          group.saved =
            jobBriefDictionary[jobBrief.draftId]

          _.pull(draftIds, jobBrief.draftId)
        }
      }

      jobBriefs.push(group)
    }
  })

  // add drafts with no published (as saved)
  draftIds.map(draftId => {
    jobBriefs.push({
      id: draftId,
      saved: jobBriefDictionary[draftId]
    })
  })

  return {
    jobBriefs,
    qualifications
  }
}

async function jobBriefInfos (ids) {

  let jobBriefs = await sequelizeHelper.fromSql(
    dataConfig.output.jobBriefInfos(ids)
  )
  .catch((err) => console.log(err))

  jobBriefs = sequelizeToJSON(jobBriefs)

  const result =
    await processJobBriefInfos(jobBriefs, true)

  result.qualifications =
    await qualificationInfos(result.qualifications)

  return result
}
async function jobBriefDetails (ids) {

  let rows = await sequelizeHelper.fromSql(
    dataConfig.output.jobBriefDetails(ids)
  )
  .catch((err) => console.log(err))

  const draftIds = []
  // get drafts
  _.forEach(rows, row => {
    if (row.draft.length > 0) {
      draftIds.push(row.draft[0].id)
    }
  })
  let drafts = await sequelizeHelper.fromSql(
    dataConfig.output.jobBriefDetails(draftIds)
  )
  .catch((err) => console.log(err))

  rows.push(...drafts)

  rows = sequelizeToJSON(rows)

  _.forEach(rows, (job, idx) => {
    job.updatedAt = Date.parse(job.updatedAt)
    job.publishedAt = Date.parse(job.publishedAt)
    job.endDate = Date.parse(job.endDate)

    // process images
    job.images = job.jobImage
    delete job.jobImage
    _.forEach(job.images, image => {
      image.createdAt = Date.parse(image.createdAt)
      image.updatedAt = Date.parse(image.updatedAt)
    })
  })

  const result =
    await processJobBriefInfos(rows)

  result.qualifications =
    await qualificationInfos(result.qualifications)

  return result
}

async function setJobBriefDetails (data, groupId, deleteId) {

  if (data) {
    // get existing to be used to remove unused
    let existing = await sequelizeHelper.fromSql(
      dataConfig.output
        .jobBriefDetails(data.id)
    )
    .catch((err) => console.log(err))
    existing = sequelizeToJSON(existing)

    const entry = _.pick(data, [
      'id', 'name', 'description',
      'status', 'type',
      'publishedAt',
      'startDate',
      'endDate',
    ])

    sequelizeHelper.db['job']
      .upsert(entry)

    // old roles to be removed if unused
    const unusedRoles = {}
    if (existing) {
      _.forEach(existing.jobRole, role => {
        unusedRoles[role.id] = role
      })
    }

    _.forEach(data.roles, role => {

      const roleEntry = _.pick(role, [
        'id', 'name', 'description',
        'needed'
      ])

      roleEntry.jobId = data.id

      sequelizeHelper.db['jobRole']
        .upsert(roleEntry)

      // old qualifications to be removed if unused
      const existingRole = unusedRoles[role.id]
      let unusedQs
      if (existingRole) {
        unusedQs = existingRole.qualifications
      }

      // remove role from unusedRoles
      delete unusedRoles[role.id]

      _.forEach(role.qualifications, q => {

        const qEntry = {
          jobRole: role.id,
          qualification: q.id,
          level: q.level,
        }

        sequelizeHelper.db['jobRoleQualification']
          .upsert(qEntry)

        if (unusedQs) {
          // remove qualification from unusedQs
          delete unusedQs[q.id]
        }
      })

      if (unusedQs) {
        // remove unused qualifications
        _.forEach(unusedQs, q => {
          sequelizeHelper.db['jobRoleQualification']
            .destroy({where: {qualification: q.id}})
        })
      }
    })

    // remove unused roles
    _.forEach(unusedRoles, role => {
      sequelizeHelper.db['jobRole']
        .destroy({where: {id: role.id}})
    })

    if (
      groupId &&
      groupId !== data.id
    ) {
      sequelizeHelper.db['draftAndPublishedJobBrief']
        .upsert({
          draft: data.id,
          published: groupId
        })
    }
  }

  sequelizeHelper.db['job']
    .destroy({where: {id: deleteId}})

  await sequelizeHelper.sequelize.sync()
    .catch((err) => {
      console.log(err)
      return false
    })

  return true
}

async function deleteJobBrief (id) {

}

// RECRUITMENT
async function processRecruitmentInfos (result, getDetails = false) {

  let freelancerIds = []

  _.forEach(result.jobBriefs, (jobBrief, idx) => {

    jobBrief.saved = jobBrief.published
    delete jobBrief.published
    jobBrief = jobBrief.saved

    _.forEach(jobBrief.roles, role => {

      const newRole = _.clone(role)
      newRole.nPotential = 99

      newRole.applicants =
        arrayToOrderedObject(role.jraFreelancerId)
      delete newRole.jraFreelancerId

      _.forEach(newRole.applicants, freelancer => {
        _.assign(freelancer, freelancer.jobRoleApplication)
        delete freelancer.jobRoleApplication

        freelancer.createdAt = Date.parse(freelancer.createdAt)
      })

      // collect freelancerIds
      freelancerIds = _.union(
        freelancerIds,
        _.keys(newRole.applicants)
      )

      // mark "signed"
      _.forEach(newRole.sfFreelancerId, freelancer => {
        newRole.applicants[freelancer.id].signed = true
      })
      delete newRole.sfFreelancerId

      jobBrief.roles[role.id] = newRole
    })

    delete jobBrief.jobRole
  })

  // process freelancers
  if(!getDetails){
    result.freelancers = await sequelizeHelper.fromSql(
      dataConfig.output.freelancerRecruitments(freelancerIds)
    )
    .catch((err) => console.log(err))
  }
  else {
    result.freelancers = await sequelizeHelper.fromSql(
      dataConfig.output.freelancerRecruitmentDetails(freelancerIds)
    )
    .catch((err) => console.log(err))
  }

  result.freelancers = sequelizeToJSON(result.freelancers)

  _.forEach(result.freelancers, freelancer => {

    freelancer.qualifications =
      arrayToOrderedObject(freelancer.qualification)
    delete freelancer.qualification

    _.forEach(freelancer.qualifications, qualification => {
      _.assign(qualification, qualification.freelancerQualification)
      delete qualification.freelancerQualification
    })

    // collect qualifications
    result.qualifications = _.union(
      result.qualifications,
      _.keys(freelancer.qualifications)
    )

    // if getting details, do the rest
    if(getDetails){
      freelancer.portfolios = freelancer.portfolio
      delete freelancer.portfolio
    }
  })

  return result
}

async function recruitmentInfos (ids) {

  let recruitments = await sequelizeHelper.fromSql(
    dataConfig.output.recruitmentInfos(ids)
  )
  .catch((err) => console.log(err))

  recruitments = sequelizeToJSON(recruitments)
  let result =
    await processJobBriefInfos(recruitments)
  result =
    await processRecruitmentInfos(result)

  result.qualifications =
    await qualificationInfos(result.qualifications)

  result.recruitments = result.jobBriefs
  delete result.jobBriefs
  return result
}
async function recruitmentDetails (ids, getRefs = true) {

  let recruitmentDetails = await sequelizeHelper.fromSql(
    dataConfig.output.recruitmentDetails(ids)
  )
  .catch((err) => console.log(err))

  recruitmentDetails = sequelizeToJSON(recruitmentDetails)
  let result =
    await processJobBriefInfos(recruitmentDetails, true)
  result =
    await processRecruitmentInfos(result, getRefs)

  if (getRefs) {
    result.qualifications =
      await qualificationInfos(result.qualifications)
  }

  result.recruitments = result.jobBriefs
  delete result.jobBriefs
  return result
}

async function setRecruitmentDetails (data) {

  // get existing
  let existing = await recruitmentDetails(data.id, false)
  existing = _.get(existing, ['recruitments', 0, 'saved'])

  _.forEach(data.roles, role => {

    const existingRole = existing
      ? existing.roles[role.id]
      : undefined

    _.forEach(role.applicants, a => {

      const existingA = existingRole
        ? existingRole.applicants[a.id]
        : {}

      const aEntry = {
        jraJobRole: role.id,
        jraFreelancerId: a.id,
        shortlisted: a.shortlisted,
        removed: a.removed,
      }

      sequelizeHelper.db['jobRoleApplication']
        .upsert(aEntry)

      // if signed, and existing is not, upsert signed
      if (a.signed && !existingA.signed) {
        sequelizeHelper.db['signedFreelancer']
          .upsert({
            sfJobRole: role.id,
            sfFreelancerId: a.id,
          })
      }
      // if the opposite, destroy entry
      else if (!a.signed && existingA.signed) {
        sequelizeHelper.db['signedFreelancer']
          .destroy({
            where: {
              sfJobRole: role.id,
              sfFreelancerId: a.id,
            }
          })
      }
    })
  })

  await sequelizeHelper.sequelize.sync()
    .catch((err) => {
      console.log(err)
      return false
    })

  return true
}

// FREELANCER
async function processFreelancerInfos (freelancerList) {

  const freelancers = {}
  let qualifications = []
  _.forEach(freelancerList, freelancer => {

    // move level from freelancerQualification
    // and convert to searchable object
    freelancer.qualifications =
      arrayToOrderedObject(freelancer.qualification)
    delete freelancer.qualification

    _.forEach(freelancer.qualifications, (qualification, idx) => {

      _.assign(
        qualification,
        qualification.freelancerQualification
      )

      delete qualification.freelancerQualification
    })

    // collect qualifications
    qualifications = _.union(
      qualifications,
      _.keys(freelancer.qualifications)
    )

    freelancer.updatedAt = Date.parse(freelancer.updatedAt)

    // put to saved group
    freelancers[freelancer.id] = {
      id: freelancer.id,
      saved: freelancer
    }
  })

  return {
    freelancers,
    qualifications
  }
}

async function freelancerInfos (ids) {

  let freelancers = await sequelizeHelper.fromSql(
    dataConfig.output.freelancerInfos(ids)
  )
  .catch((err) => console.log(err))

  freelancers = sequelizeToJSON(freelancers)

  const result = await processFreelancerInfos(freelancers)

  result.qualifications =
    await qualificationInfos(result.qualifications)

  return result
}
async function freelancerDetails (ids) {

  let freelancers = await sequelizeHelper.fromSql(
    dataConfig.output.freelancerDetails(ids)
  )
  .catch((err) => console.log(err))

  freelancers = sequelizeToJSON(freelancers)

  const result = await processFreelancerInfos(freelancers)

  _.forEach(result.freelancers, freelancer => {

    freelancer = freelancer.saved

    freelancer.portfolios = freelancer.portfolio
    delete freelancer.portfolio

    // convert dates

    _.forEach(freelancer.portfolios, portfolio => {

      portfolio.createdAt =
        Date.parse(portfolio.createdAt)

      portfolio.updatedAt =
        Date.parse(portfolio.updatedAt)
    })

    freelancer.birthDate =
      Date.parse(freelancer.birthDate)

    _.forEach(freelancer.qualifications, qualification => {

      qualification.updatedAt =
        Date.parse(qualification.updatedAt)

      qualification.updatedByFreelancerAt =
        Date.parse(qualification.updatedByFreelancerAt)

      qualification.lastCheckedAt =
        Date.parse(qualification.lastCheckedAt)
    })
  })

  result.qualifications =
    await qualificationInfos(result.qualifications)

  return result
}

async function setFreelancerDetails (details) {
}
async function setFreelancerByAdmin (details) {

  _.forEach(details.qualifications, q => {
    const newQ = _.pick(q, [
      'id', 'claimed', 'confirmed',
      'updatedByFreelancerAt',
      'lastCheckedAt',
      'supportRequest'
    ])
    newQ.freelancer = details.id
    sequelizeHelper.db['freelancerQualification']
      .upsert(newQ)
  })

  await sequelizeHelper.sequelize.sync()
    .catch((err) => {
      console.log(err)
      return false
    })

  return true
}

// QUALIFICATIONS
async function qualificationInfos (ids) {

  qualifications = await sequelizeHelper.fromSql(
    dataConfig.output.qualificationInfos(ids)
  )
  .catch((err) => console.log(err))

  return sequelizeToJSON(qualifications)
}

async function searchQualifications (text) {
  qualifications = await sequelizeHelper.fromSql(
    dataConfig.output.searchQualifications(text)
  )
  .catch((err) => console.log(err))

  return sequelizeToJSON(qualifications)
}

const dataRequests = {
  jobBriefInfos, jobBriefDetails,

  recruitmentInfos, recruitmentDetails,

  freelancerInfos, freelancerDetails,

  qualificationInfos, searchQualifications,
}

const dataStorings = {
  setJobBriefDetails,
  setRecruitmentDetails,
  setFreelancerDetails, setFreelancerByAdmin
}
