module.exports = {
  folders: {
    classes: '19EFoTq1XO1m66HIs_Ih0eF0S3kgTKFpv',
    joins: '1_Yoo6TTCi3G8kZAXDO3MnnxoBTKRGyYC',
    ad_hoc_sheet:'1R74BuyEhQfzOi3-wF0IwkCNx4NsPdX_-',
  },
  files: {
    project: '1h2wVcoZNt5wh9_KGokKnnkSfiinuiUanY0Ars8pLfNY',
    project_phase: '1_5tzRFrfE1Mfay9BU0oUjDfCXrpmwg8Rm5cd4L6cB64',
    project_team: '',

    // ad hoc sheets
    project_to_team: '1aNtEm5_8kpAvQ_L0NZGHjPBAL9MdBzMXtGfBK96KmOA'
  }
}
