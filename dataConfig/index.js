/*
data types: string, integer, float, bool, date, datetime
 */

module.exports = {
  classes: require('./classes'),
  joins: require('./joins'),
  output: require('./output'),
  input: require('./input'),
}
