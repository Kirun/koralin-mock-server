const Op = require ('sequelize').Op

function addIds (ids, query) {
  if(ids){
    if(typeof ids == 'string'){
      query.where = {
        id: ids
      }
    }
    else {
      query.where = {
        id: {
          [Op.or]: ids
        }
      }
    }
  }
  return query
}

// JOB BRIEF
function jobBriefInfos (ids) {
  return addIds(ids, {
    class: 'job',
    columns: [
      'id',
      'name', 'description',
      'status',
      'startDate',
      'updatedAt'
    ],

    include: [
      {
        class: 'job',
        as: 'draft',
        columns: [
          'id',
        ],
        join: {
          columns: []
        },
      },
      {
        class: 'jobRole',
        as: 'jobRole',
        columns: [
          'id',
          'name',
          'needed'
        ],
        include: [
          {
            class: 'qualification',
            as: 'qualification',
            join: {
              columns: [
                'level'
              ]
            },
            columns: [
              'id',
            ]
          },
        ]
      }
    ]
  })
}
function jobBriefDetails (ids) {
  const query = jobBriefInfos(ids)

  query.columns.push(...[
    'type',
    'publishedAt',
    'endDate',
  ])

  // roles
  query.include[1].columns.push('description')

  // images
  // query.include.push({
  //   class: 'image',
  //   as: 'jobImage',
  //   columns: [
  //     'url',
  //     'createdAt',
  //     'updatedAt'
  //   ],
  // })

  return query
}

// RECRUITMENT
function recruitmentInfos (ids) {
  return addIds(ids, {

    class: 'job',

    where: {
      status: 'published'
    },

    columns: [
      'id',
      'name',
      'startDate',
      'status',
    ],

    include: [
      {
        class: 'jobRole',
        as: 'jobRole',
        columns: [
          'id',
          'name',
          'needed'
        ],
        include: [
          {
            class: 'qualification',
            as: 'qualification',
            join: {
              columns: [
                'level',
              ]
            },
            columns: [
              'id'
            ]
          },
          {
            class: 'freelancer',
            as: 'jraFreelancerId',
            join: {
              columns: [
                'shortlisted',
                'removed',
                'updatedAt'
              ]
            },
            columns: ['id'],
          },
          {
            class: 'freelancer',
            as: 'sfFreelancerId',
            join: {
              columns: [
                'createdAt'
              ]
            },
            columns: ['id']
          },
        ]
      },
      {
        class: 'job',
        as: 'draft',
        columns: [
          'id'
        ],
        join: {
          columns: []
        }
      }
    ]
  })
}
function freelancerRecruitments (ids) {
  return addIds(ids, {

    class: 'freelancer',

    where: {
      status: 'published'
    },

    columns: [
      'id',
    ],

    include: [
      {
        class: 'qualification',
        as: 'qualification',
        join: {
          columns: [
            'claimed',
            'confirmed',
            'updatedAt',
          ]
        },
        columns: [
          'id',
        ]
      },
    ]
  })
}

function recruitmentDetails (ids) {
  const query = recruitmentInfos(ids)

  query.columns.push(...[
    'description',
  ])
  // job role
  query.include[0].columns.push('description')
  // job role / freelancer(applicant)
  query.include[0].include[1].join.columns.push('createdAt')

  return query
}
function freelancerRecruitmentDetails (ids) {
  const query = freelancerRecruitments(ids)

  query.columns.push(...[
    'name'
  ])

  query.include.push({
    class: 'portfolio',
    as: 'portfolio',
    columns: [
      'url',
      'online',
      'type',
    ],
  })
  return query
}

// FREELANCER
function freelancerInfos (ids) {
  return addIds(ids, {

    class: 'freelancer',

    columns: [
      'id',
      'name',
      'email',
      'updatedAt',
    ],

    include: [
      {
        class: 'qualification',
        as: 'qualification',
        join: {
          columns: [
            'claimed',
            'confirmed',
            'updatedAt',
          ]
        },
        columns: [
          'id',
        ]
      },
    ]
  })
}
function freelancerDetails (ids) {
  const query = freelancerInfos(ids)

  query.columns.push(...[
    'phone',
    'domicile',
    'birthDate',
  ])

  // qualification
  query.include[0].join.columns.push(...[
    'supportRequest',
    'updatedByFreelancerAt',
    'lastCheckedAt',
  ])

  query.include.push({
    class: 'portfolio',
    as: 'portfolio',
    columns: [
      'url',
      'online',
      'type',
      'createdAt',
      'updatedAt'
    ],
  })

  return query
}


// QUALIFICATIONS
function qualificationInfos (ids) {
  return addIds(ids, {

    class: 'qualification',

    columns: [
      'id',
      'name',
      'category',
    ],
  })
}
function searchQualifications (text) {
  return {

    class: 'qualification',

    where: {
      l_name: {
        [Op.like]: '%'+text+'%'
      }
    },

    columns: [
      'id',
      'name',
      'category',
    ],
  }
}

module.exports = {
  // JOB BRIEF
  jobBriefInfos,
  jobBriefDetails,

  // RECRUITMENT
  recruitmentInfos,
  freelancerRecruitments,
  recruitmentDetails,
  freelancerRecruitmentDetails,

  // FREELANCER
  freelancerInfos,
  freelancerDetails,

  // QUALIFICATIONS
  qualificationInfos,
  searchQualifications,
}
