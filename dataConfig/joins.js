module.exports = {
  // job stuff
  draftAndPublishedJobBrief: {
    columns: [
      'draft',
      'published',
    ],
    joins: {
      draft: 'job',
      published: 'job',
    },
    config: {
      draft: {type: 'string', primaryKey: true},
      published: {type: 'string', primaryKey: true},
    }
  },
  jobRoleQualification: {
    columns: [
      'jobRole', 'qualification',
      'level', 'isOptional'
    ],
    joins: {
      jobRole: 'jobRole',
      qualification: 'qualification',
    },
    config: {
      jobRole: {type: 'string', primaryKey: true},
      qualification: {type: 'string', primaryKey: true},
      level: {
        type: 'integer',
        defaultValue: 1
      },
      isOptional: {
        type: 'bool',
        defaultValue: false
      },
    }
  },

  jobRoleApplication: {
    columns: [
      'jraJobRole', 'jraFreelancerId',
      'shortlisted', 'removed'
    ],
    joins: {
      jraJobRole: 'jobRole',
      jraFreelancerId: 'freelancer',
    },
    config: {
      jraJobRole: {type: 'string', primaryKey: true},
      jraFreelancerId: {type: 'string', primaryKey: true},
      shortlisted: {
        type: 'bool',
        defaultValue: false
      },
      removed: {
        type: 'bool',
        defaultValue: false
      },
    }
  },
  signedFreelancer: {
    columns: [
      'sfJobRole', 'sfFreelancerId',
    ],
    joins: {
      sfJobRole: 'jobRole',
      sfFreelancerId: 'freelancer',
    },
    config: {
      sfJobRole: {type: 'string', primaryKey: true},
      sfFreelancerId: {type: 'string', primaryKey: true},
    }
  },
  // freelancer stuff
  freelancerQualification: {
    columns: [
      'freelancer', 'qualification',
      'claimed', 'confirmed',
      'updatedByFreelancerAt', 'lastCheckedAt',
      'supportRequest',
    ],
    joins: {
      freelancer: 'freelancer',
      qualification: 'qualification',
    },
    config: {
      freelancer: {type: 'string', primaryKey: true},
      qualification: {type: 'string', primaryKey: true},
      claimed: {
        type: 'integer',
        defaultValue: 1
      },
      confirmed: {
        type: 'integer',
        defaultValue: 1
      },
      updatedByFreelancerAt: {type: 'date'},
      lastCheckedAt: {type: 'date'},
      supportRequest: {type: 'bool'},
    }
  },
}
