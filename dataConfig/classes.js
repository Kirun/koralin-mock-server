module.exports = {
  // job stuff
  job: {
    columns: [
      'id', 'name', 'client', 'type',
      'status',
      'description',
      'status', 'publishedAt',
      'startDate', 'endDate'
    ],
    config: {
      id: {type: 'string'},
      name: {type: 'string'},
      client: {type: 'string'},
      type: {type: 'string'},

      status: {
        type: 'string',
        defaultValue: 'draft'
      },
      
      description: {type: 'string'},

      publishedAt: {type: 'datetime'},

      startDate: {
        type: 'date',
        allowNull: true,
      },
      endDate: {
        type: 'date',
        allowNull: true,
      },
    },
  },
  image: {
    columns: [
      'url', 'jobId',
    ],
    config: {
      url: {type: 'string', primaryKey: true},
      jobId: {type: 'string', primaryKey: true},
    },
    manyBelongsTo: [
      {
        class: 'job',
        foreignKey: 'jobId',
        sourceAlias: 'jobImage',
        targetAlias: 'job',
      },
    ]
  },

  // freelancer stuff
  jobRole: {
    columns: [
      'id', 'jobId',
      'status', 'otherVersion',
      'name', 'roleId',
      'description',
      'needed'
    ],
    config: {
      id: {type: 'string'},
      jobId: {type: 'string'},

      status: {
        type: 'string',
        defaultValue: 'draft'
      },
      otherVersion: {type: 'string'},

      name: {type: 'string'},
      description: {type: 'string'},
      needed: {type: 'integer'},
    },
    manyBelongsTo: [
      {
        class: 'job',
        foreignKey: 'jobId',
        sourceAlias: 'jobRole',
        targetAlias: 'job',
      },
    ]
  },
  qualification: {
    columns: [
      'id', 'name', 'l_name',
      'category'
    ],
    config: {
      id: {type: 'string'},
      name: {type: 'string'},
      l_name: {type: 'string'},
      category: {type: 'string'},
    },
  },
  freelancer: {
    columns: [
      'id', 'name',
      'email', 'phone',
      'birthDate', 'domicile',
    ],
    config: {
      id: {type: 'string'},
      name: {type: 'string'},
      email: {type: 'string'},
      phone: {type: 'string'},
      birthDate: {type: 'date'},
      domicile: {type: 'string'},
    },
  },
  portfolio: {
    columns: [
      'url',
      'freelancerId',
      'online',
      'type',
    ],
    config: {
      url: {type: 'string', primaryKey: true},
      freelancerId: {type: 'string'},
      online: {type: 'bool'},
      type: {type: 'string'},
    },
    manyBelongsTo: [
      {
        class: 'freelancer',
        foreignKey: 'freelancerId',
        sourceAlias: 'portfolio',
        targetAlias: 'freelancer',
      }
    ]
  },
}
